package com.freeformers.app.service;

import com.freeformers.app.domain.SkillSet;
import com.freeformers.app.domain.Tuple;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by issamhammi on 10/03/2018.
 * This class reads and provide a list of SkillSet from the csv file provided as an example
 * Also write output to a csv file
 */
public class IOReport {

    private final static Logger logger = Logger.getLogger(IOReport.class);

    private final static String DATASOURCE = "datasource.csv";

    private final static String OUTPUT = "report.csv";

    private final static String HEADER = "area,attribute,average_improvement\n";

    private final static String SPLIT = ",";

    public List<SkillSet> readCSV() {
        List<SkillSet> skillsets = new ArrayList();

        final ClassLoader classLoader = getClass().getClassLoader();
        final File file = new File(classLoader.getResource(DATASOURCE).getFile());
        try (
                final InputStream inputStream = new FileInputStream(file);
                final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))
        ) {
            skillsets = bufferedReader.lines().skip(1).map(convertRawDataToSkillSet).collect(Collectors.toList());
        } catch (IOException e) {
            logger.error(String.format("Unable to open datasource file: %s", e.getMessage()));
        }
        return skillsets;
    }

    public Function<String, SkillSet> convertRawDataToSkillSet = (entry) -> {
        String[] entries = entry.split(SPLIT);
        SkillSet skillset = new SkillSet();
        if (entries[0] != null)
            try {
                skillset.setUserId(Integer.parseInt(entries[0]));
            } catch (NumberFormatException e) {
                logger.error(String.format("Unable to parse userId into Integer: %s", e.getMessage()));
            }
        if (entries[1] != null)
            skillset.setArea(entries[1]);
        if (entries[2] != null)
            skillset.setAttribute(entries[2]);
        if (entries[3] != null)
            try {
                skillset.setScore(Integer.parseInt(entries[3]));
            } catch (NumberFormatException e) {
                logger.error(String.format("Unable to parse score into Integer: %s", e.getMessage()));
            }
        return skillset;
    };

    public void writeCSV(Map<Tuple, Double> map) {

        try (
                final OutputStream outputStream = new FileOutputStream(OUTPUT);
                final BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream))
        ) {
            bufferedWriter.write(HEADER);
            map.forEach((k, v) -> {
                try {
                    bufferedWriter.write(String.format("%s, %s, %.02f\n", k.getS(), k.getT(), v));
                } catch (IOException e) {
                    logger.error(String.format("Unable to write in file: %s", e.getMessage()));
                }
            });

        } catch (IOException e) {
            logger.error(String.format("Unable to create output file: %s", e.getMessage()));
        }

    }
}
