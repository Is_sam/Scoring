package com.freeformers.app.service;

import com.freeformers.app.domain.SkillSet;
import com.freeformers.app.domain.Triplet;
import com.freeformers.app.domain.Tuple;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by issamhammi on 11/03/2018.
 * Compute the report
 */
public class ImprovementReport {

    private final static Logger logger = Logger.getLogger(ImprovementReport.class);

    /**
     * get the list of score per user, per area and per attribute
     * exemple of 1 element <(8, "technology", "tooling"),[70, 72]>
     *
     * @param skillSets
     * @return Map of triplets with their list of score
     */
    public Map<Triplet, List<Integer>> getListOfScoresPerUserPerAreaPerAttribute(List<SkillSet> skillSets) {
        return skillSets.stream().collect(
                Collectors.groupingBy(s -> new Triplet(s.getUserId(), s.getArea(), s.getAttribute()),
                        Collectors.mapping(s -> s.getScore(), Collectors.toList())
                )
        );
    }

    /**
     * get improvement per area, per attributes
     * example of 1 element <("technology", "tooling"), 2>
     *
     * @param skillSetsPerUser
     * @return Map of tuple with average score
     */
    public Map<Tuple, Double> getImprovementsPerAreaPerAttributes(Map<Triplet, List<Integer>> skillSetsPerUser) {
        return skillSetsPerUser.entrySet().stream().collect(
                Collectors.groupingBy(s -> new Tuple(s.getKey().getT(), s.getKey().getU()),
                        Collectors.averagingInt(s -> computeImprovements(s.getValue()))
                )
        );
    }

    /**
     * compute improvement for a single user based on the following strategy
     * Given an array of score: [50, 54, 60, 55, 57]
     * we calculate the gap between two consecutive scores: [4, 6, -5, 2]
     * then we sum and return the elements in the array: 7
     *
     * @param scores
     * @return improvement
     */
    public static Integer computeImprovements(List<Integer> scores) {
        Integer improvement = 0;
        for (int i = 1; i < scores.size(); i++) {
            improvement += scores.get(i) - scores.get(i - 1);
        }
        return improvement;
    }
}
