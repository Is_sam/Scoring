package com.freeformers.app;

import com.freeformers.app.domain.SkillSet;
import com.freeformers.app.domain.Triplet;
import com.freeformers.app.domain.Tuple;
import com.freeformers.app.service.IOReport;
import com.freeformers.app.service.ImprovementReport;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Map;

/**
 * Created by issamhammi on 10/03/2018.
 */
public class Solution {

    private final static Logger logger = Logger.getLogger(Solution.class);

    public static void main(String[] args) {

        //basic configurator for log4j
        BasicConfigurator.configure();

        IOReport iOReport = new IOReport();
        List<SkillSet> skillSets = iOReport.readCSV();

        logger.info("CSV has been loaded");

        ImprovementReport improvementReport = new ImprovementReport();
        Map<Triplet, List<Integer>> mergedResults = improvementReport.getListOfScoresPerUserPerAreaPerAttribute(skillSets);
        Map<Tuple, Double> report = improvementReport.getImprovementsPerAreaPerAttributes(mergedResults);

        logger.info("Report generated, here is a quick preview:");

        report.forEach((k, v) -> logger.info(String.format("%s, %s, %.02f", k.getS(), k.getT(), v)));

        iOReport.writeCSV(report);
        logger.info("Ouput has been generated in the root directory");

        System.exit(0);
    }
}
