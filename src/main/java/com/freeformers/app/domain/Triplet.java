package com.freeformers.app.domain;

/**
 * Created by issamhammi on 11/03/2018.
 * Generic class with 3 elements
 */
public class Triplet<S, T, U> {

    private S s;

    private T t;

    private U u;

    public Triplet(S s, T t, U u) {
        this.s = s;
        this.t = t;
        this.u = u;
    }

    public S getS() {
        return s;
    }

    public void setS(S s) {
        this.s = s;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public U getU() {
        return u;
    }

    public void setU(U u) {
        this.u = u;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Triplet<?, ?, ?> triplet = (Triplet<?, ?, ?>) o;

        if (s != null ? !s.equals(triplet.s) : triplet.s != null) return false;
        if (t != null ? !t.equals(triplet.t) : triplet.t != null) return false;
        return u != null ? u.equals(triplet.u) : triplet.u == null;
    }

    @Override
    public int hashCode() {
        int result = s != null ? s.hashCode() : 0;
        result = 31 * result + (t != null ? t.hashCode() : 0);
        result = 31 * result + (u != null ? u.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Triplet{" +
                "s=" + s +
                ", t=" + t +
                ", u=" + u +
                '}';
    }
}
