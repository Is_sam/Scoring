package com.freeformers.app.domain;

/**
 * Created by issamhammi on 11/03/2018.
 * Generic class with 2 elements
 */
public class Tuple<S, T> {

    private S s;

    private T t;

    public Tuple(S s, T t) {
        this.s = s;
        this.t = t;
    }

    public S getS() {
        return s;
    }

    public void setS(S s) {
        this.s = s;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tuple<?, ?> tuple = (Tuple<?, ?>) o;

        if (s != null ? !s.equals(tuple.s) : tuple.s != null) return false;
        return t != null ? t.equals(tuple.t) : tuple.t == null;
    }

    @Override
    public int hashCode() {
        int result = s != null ? s.hashCode() : 0;
        result = 31 * result + (t != null ? t.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Tuple{" +
                "s=" + s +
                ", t=" + t +
                '}';
    }
}
