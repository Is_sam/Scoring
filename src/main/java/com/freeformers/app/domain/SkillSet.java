package com.freeformers.app.domain;

/**
 * Created by issamhammi on 10/03/2018.
 */
public class SkillSet {

    private Integer userId;

    private String area;

    private String attribute;

    private Integer score;

    public SkillSet() {
    }

    public SkillSet(Integer userId, String area, String attribute, Integer score) {
        this.userId = userId;
        this.area = area;
        this.attribute = attribute;
        this.score = score;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SkillSet skillset = (SkillSet) o;

        if (userId != null ? !userId.equals(skillset.userId) : skillset.userId != null) return false;
        if (area != null ? !area.equals(skillset.area) : skillset.area != null) return false;
        if (attribute != null ? !attribute.equals(skillset.attribute) : skillset.attribute != null) return false;
        return score != null ? score.equals(skillset.score) : skillset.score == null;
    }

    @Override
    public int hashCode() {
        int result = userId != null ? userId.hashCode() : 0;
        result = 31 * result + (area != null ? area.hashCode() : 0);
        result = 31 * result + (attribute != null ? attribute.hashCode() : 0);
        result = 31 * result + (score != null ? score.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SkillSet{" +
                "userId=" + userId +
                ", area='" + area + '\'' +
                ", attribute='" + attribute + '\'' +
                ", score=" + score +
                '}';
    }
}
