package com.freeformers.app.service;

import com.freeformers.app.domain.SkillSet;
import com.freeformers.app.domain.Triplet;
import com.freeformers.app.domain.Tuple;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by issamhammi on 11/03/2018.
 */
public class ImprovementReportTest {


    private List<SkillSet> skillSets;
    private Map<Triplet, List<Integer>> expectedSkillSetPerUserPerAreaPerAttributes;
    private Map<Tuple, Double> expectedImprovementReport;


    private ImprovementReport improvementReport;

    @Before
    public void setup() {

        skillSets = new ArrayList<>();
        skillSets.add(new SkillSet(8, "technology", "forward_thinking", 63));
        skillSets.add(new SkillSet(8, "technology", "forward_thinking", 67));
        skillSets.add(new SkillSet(9, "technology", "forward_thinking", 70));
        skillSets.add(new SkillSet(9, "technology", "forward_thinking", 70));

        expectedSkillSetPerUserPerAreaPerAttributes = new HashMap();
        expectedSkillSetPerUserPerAreaPerAttributes
                .put(
                        new Triplet(8, "technology", "forward_thinking"),
                        Arrays.asList(63, 67)
                );
        expectedSkillSetPerUserPerAreaPerAttributes
                .put(
                        new Triplet(9, "technology", "forward_thinking"),
                        Arrays.asList(70, 70)
                );

        expectedImprovementReport = new HashMap<>();
        expectedImprovementReport.put(
                new Tuple("technology", "forward_thinking"),
                2D
        );

        improvementReport = new ImprovementReport();
    }

    @Test
    public void getListOfScoresPerUserPerAreaPerAttribute() {
        assertEquals(
                expectedSkillSetPerUserPerAreaPerAttributes,
                improvementReport.getListOfScoresPerUserPerAreaPerAttribute(skillSets)
        );
    }

    @Test
    public void getImprovementsPerAreaPerAttributes() {
        assertEquals(
                expectedImprovementReport,
                improvementReport.getImprovementsPerAreaPerAttributes(expectedSkillSetPerUserPerAreaPerAttributes)
        );
    }

    @Test
    public void computeImprovements() {
        assertEquals(Integer.valueOf(0), ImprovementReport.computeImprovements(Arrays.asList(92038)));
        assertEquals(Integer.valueOf(0), ImprovementReport.computeImprovements(Arrays.asList(70, 70, 70, 70)));
        assertEquals(Integer.valueOf(10), ImprovementReport.computeImprovements(Arrays.asList(10, 12, 14, 16, 18, 20)));
        assertEquals(Integer.valueOf(0), ImprovementReport.computeImprovements(Arrays.asList(10, 12, 10, 16, 10, 20, 10)));
        assertEquals(Integer.valueOf(-2), ImprovementReport.computeImprovements(Arrays.asList(2, 12, 8, 14, 0)));
    }

}